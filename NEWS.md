# g2.r.results 0.0.0.9000

* First realease of the package
* Added a `NEWS.md` file to track changes to the package.

## TODO

* Drop trait transformations (like lnFXIc)
* Create a pipeline of work with `redo` [web1](https://github.com/karoliskoncevicius/r_redo_example) and [web2](http://karolis.koncevicius.lt/posts/using_redo_to_manage_r_data_analysis_workflow/)
* Create a forecast of VT prognosis using [this workflow](https://thierrymoudiki.github.io/blog/2020/10/02/misc/r/osic-kaggle)
* Look in `kaggle` for possible usable databases: [kabble pulmonary](https://www.kaggle.com/c/osic-pulmonary-fibrosis-progression)
* For example [Venous Thromboembolism Risk (VTE risk)](https://www.kaggle.com/mpwolke/cusersmarildownloadsthromboembolismcsv)
* Use the concept of n-gram of the tidytext package in gen mutations (mutations in order). This can be very usefull for a particular gen, for example, visualize the differences between cases and controls in n-grams


t2 <- train_txt %>% select(ID, txt) %>% unnest_tokens(bigram, txt, token = "ngrams", n = 2)
https://www.kaggle.com/headsortails/personalised-medicine-eda-with-tidy-r


## thesaurus https://www.kaggle.com/merckel/nci-thesaurus-naive-bayes-vs-rf-gbm-glm-dl
## https://evs.nci.nih.gov/ftp1/NCI_Thesaurus/ReadMe.txt



https://www.kaggle.com/nonserial/covid19-mlr3learners-lightgbm-on-daily-new-cases


