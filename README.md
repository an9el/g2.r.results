GAIT2 results collector
================
:pencil: Angel Martinez-Perez [![ORCID
ID](pictures/ORCIDiD_icon24x24.png)](https://orcid.org/0000-0002-5934-1454)









<!-- README.md is generated from README.Rmd. Please edit that file -->

-----

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![Last-changedate](https://img.shields.io/badge/last%20change-2020--09--25-yellowgreen.svg)](/commits/master)
<!-- badges: end -->

\[\[*TOC*\]\]

``` r
f1 <- g2.s.pheno()
## f1$getTraits('analizables')
```

1.  ([Vila, et al., 2010](#bib-Vila2010))

# Collect results

For example for collect all results from gwr with pval = 10e-3. First
create the “quick” object for quick query

``` r

all <- g2.s.result.gwr(pheno = NULL, quality.gwr = 'quick', pval = 1e-3, Est.SE = 10, n.alt = 5, MAC = 10, verbose = FALSE)
save(all, file = 'g2.r.gwr.all.0.001.RData')


for(i in seq_along(along.with = resultados)){
      quick <- resultados[i] %>% getobj
      quick <- g2.s.gwa.region::g2.s.gwr.clean(data = quick, pval_burden = 1e-3)
      quick %>% save(file = glue::glue("g2.r.gwr.quick/{resultados2[i]}"))
}
```

# :book: Bibliography

<details open>

<summary> <span title="Click to Collapse/Expand"> REFERENCES </span>
</summary>

<a name=bib-Vila2010></a>[\[1\]](#cite-Vila2010) L. Vila et al.
“Heritability of Thromboxane A 2 and Prostaglandin E 2 Biosynthetic
Machinery in a Spanish Population”. In: *Arteriosclerosis, thrombosis,
and vascular biology* 30.1 (2010), pp. 128-134. DOI:
[10.1161/ATVBAHA.109.193219](https://doi.org/10.1161%2FATVBAHA.109.193219).
URL: <https://pubmed.ncbi.nlm.nih.gov/19850905/>.

</details>

<br>

<details closed>

<summary> <span title="Clik to Expand/Collapse"> Current session info
</span> </summary>

``` r

─ Session info ───────────────────────────────────────────────────────────────
 setting  value                       
 version  R version 4.0.2 (2020-06-22)
 os       Debian GNU/Linux 10 (buster)
 system   x86_64, linux-gnu           
 ui       X11                         
 language (EN)                        
 collate  en_US.UTF-8                 
 ctype    en_US.UTF-8                 
 tz       Europe/Madrid               
 date     2020-09-25                  

─ Packages ───────────────────────────────────────────────────────────────────
 ! package         * version  date       lib
   AnnotationDbi     1.50.3   2020-07-25 [2]
   assertthat        0.2.1    2019-03-21 [2]
   backports         1.1.10   2020-09-15 [2]
   base64enc         0.1-3    2015-07-28 [2]
   bibtex            0.4.2.2  2020-01-02 [2]
   Biobase           2.48.0   2020-04-27 [2]
   BiocGenerics      0.34.0   2020-04-27 [2]
   BiocManager       1.30.10  2019-11-16 [2]
   bit               4.0.4    2020-08-04 [2]
   bit64             4.0.5    2020-08-30 [2]
   blob              1.2.1    2020-01-20 [2]
   broom             0.7.0    2020-07-09 [2]
   callr             3.4.4    2020-09-07 [2]
   cellranger        1.1.0    2016-07-27 [2]
   checkmate       * 2.0.0    2020-02-06 [2]
   cli               2.0.2    2020-02-28 [2]
   clipr             0.7.0    2019-07-23 [2]
   cluster           2.1.0    2019-06-19 [4]
   codetools         0.2-16   2018-12-24 [4]
   colorspace        1.4-1    2019-03-18 [2]
   crayon            1.3.4    2017-09-16 [2]
   data.table        1.13.0   2020-07-24 [2]
   DBI               1.1.0    2019-12-15 [2]
   dbplyr            1.4.4    2020-05-27 [2]
   desc              1.2.0    2018-05-01 [2]
   details         * 0.2.1    2020-01-12 [2]
   devtools        * 2.3.2    2020-09-18 [2]
   digest            0.6.25   2020-02-23 [2]
   doParallel        1.0.15   2019-08-02 [2]
   dplyr           * 1.0.2    2020-08-18 [2]
   DT              * 0.15     2020-08-05 [2]
   dynamicTreeCut  * 1.63-1   2016-03-11 [2]
   ellipsis          0.3.1    2020-05-15 [2]
   evaluate          0.14     2019-05-28 [2]
   fansi             0.4.1    2020-01-08 [2]
   fastcluster     * 1.1.25   2018-06-07 [2]
   filehash          2.4-2    2019-04-17 [2]
   forcats         * 0.5.0    2020-03-01 [2]
   foreach           1.5.0    2020-03-30 [2]
   foreign           0.8-80   2020-05-24 [4]
   Formula         * 1.2-3    2018-05-03 [2]
   fs                1.5.0    2020-07-31 [2]
   fst             * 0.9.4    2020-08-27 [2]
   g2.s.gwa.region * 0.2.0    2020-09-25 [2]
   gait2           * 0.4.0    2020-09-25 [2]
   gdsfmt          * 1.24.1   2020-06-16 [2]
   generics          0.0.2    2018-11-29 [2]
   ggplot2         * 3.3.2    2020-06-19 [2]
   glue            * 1.4.2    2020-08-27 [2]
   GO.db             3.11.4   2020-06-16 [2]
   gridExtra         2.3      2017-09-09 [2]
   gtable            0.3.0    2019-03-25 [2]
   haven             2.3.1    2020-06-01 [2]
   Hmisc           * 4.4-1    2020-08-10 [2]
   hms               0.5.3    2020-01-08 [2]
   htmlTable       * 2.1.0    2020-09-16 [2]
   htmltools         0.5.0    2020-06-16 [2]
   htmlwidgets       1.5.1    2019-10-08 [2]
   httr              1.4.2    2020-07-20 [2]
   impute            1.62.0   2020-04-27 [2]
   IRanges           2.22.2   2020-05-21 [2]
   iterators         1.0.12   2019-07-26 [2]
   jpeg              0.1-8.1  2019-10-24 [2]
   jsonlite          1.7.1    2020-09-07 [2]
   knitcitations   * 1.0.10   2019-09-15 [2]
 V knitr           * 1.29     2020-09-22 [2]
   lattice         * 0.20-41  2020-04-02 [4]
   latticeExtra      0.6-29   2019-12-19 [2]
   lifecycle         0.2.0    2020-03-06 [2]
   lubridate         1.7.9    2020-06-08 [2]
   magrittr        * 1.5      2014-11-22 [2]
   Matrix            1.2-18   2019-11-27 [4]
   matrixStats       0.56.0   2020-03-13 [2]
   memoise           1.1.0    2017-04-21 [2]
   modelr            0.1.8    2020-05-19 [2]
   munsell           0.5.0    2018-06-12 [2]
   nnet              7.3-14   2020-04-26 [2]
   pacman          * 0.5.1    2019-03-11 [2]
   pillar            1.4.6    2020-07-10 [2]
   pkgbuild          1.1.0    2020-07-13 [2]
   pkgconfig         2.0.3    2019-09-22 [2]
   pkgload           1.1.0    2020-05-29 [2]
   plyr            * 1.8.6    2020-03-03 [2]
   png               0.1-7    2013-12-03 [2]
   preprocessCore    1.50.0   2020-04-27 [2]
   prettyunits       1.1.1    2020-01-24 [2]
   processx          3.4.4    2020-09-03 [2]
   ps                1.3.4    2020-08-11 [2]
   purrr           * 0.3.4    2020-04-17 [2]
   R6              * 2.4.1    2019-11-12 [2]
   RColorBrewer      1.1-2    2014-12-07 [2]
   Rcpp              1.0.5    2020-07-06 [2]
   readr           * 1.3.1    2018-12-21 [2]
   readxl            1.3.1    2019-03-13 [2]
   RefManageR      * 1.2.12   2019-04-03 [2]
   remotes           2.2.0    2020-07-21 [2]
   reprex            0.3.0    2019-05-16 [2]
   reshape2        * 1.4.4    2020-04-09 [2]
   rlang             0.4.7    2020-07-09 [2]
   rmarkdown       * 2.3      2020-06-18 [2]
   roxygen2        * 7.1.1    2020-06-27 [2]
   rpart             4.1-15   2019-04-12 [2]
   rprojroot         1.3-2    2018-01-03 [2]
   RSQLite           2.2.0    2020-01-07 [2]
   rstudioapi        0.11     2020-02-07 [2]
   rvest             0.3.6    2020-07-25 [2]
   S4Vectors         0.26.1   2020-05-16 [2]
   scales            1.1.1    2020-05-11 [2]
   sessioninfo       1.1.1    2018-11-05 [2]
   stringi           1.5.3    2020-09-09 [2]
   stringr         * 1.4.0    2019-02-10 [2]
   survival        * 3.2-3    2020-06-13 [2]
   testthat        * 2.3.2    2020-03-02 [2]
   tibble          * 3.0.3    2020-07-10 [2]
   tidyr           * 1.1.2    2020-08-27 [2]
   tidyselect        1.1.0    2020-05-11 [2]
   tidyverse       * 1.3.0    2019-11-21 [2]
   tikzDevice      * 0.12.3.1 2020-06-30 [2]
   usethis         * 1.6.3    2020-09-17 [2]
   vctrs             0.3.4    2020-08-29 [2]
   WGCNA           * 1.69     2020-02-28 [2]
 V withr             2.2.0    2020-09-22 [2]
   xfun              0.17     2020-09-09 [2]
   xml2              1.3.2    2020-04-23 [2]
   yaml              2.2.1    2020-02-01 [2]
 source                                
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 Bioconductor                          
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.0)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 gitlab (an9el/g2.s.gwa.region@de00fa9)
 gitlab (an9el/gait2@3efb770)          
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 Bioconductor                          
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 Bioconductor                          
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.2)                        
 CRAN (R 4.0.1)                        
 CRAN (R 4.0.1)                        

[1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.0
[2] /usr/local/lib/R/site-library
[3] /usr/lib/R/site-library
[4] /usr/lib/R/library

 V ── Loaded and on-disk version mismatch.
```

</details>

<br>
